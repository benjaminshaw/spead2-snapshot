#!/bin/bash
set -e

python3 gen/gen_ibv_loader.py header > include/spead2/common_ibv_loader.h
python3 gen/gen_ibv_loader.py cxx > src/common_ibv_loader.cpp
autoreconf --install --force
